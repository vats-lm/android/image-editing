package com.example.imageediting.repository;


import android.app.Application;

import com.example.imageediting.database.AppDatabase;
import com.example.imageediting.database.dao.AnnotatedPointDao;
import com.example.imageediting.database.entity.AnnotatedPoint;

import java.util.List;

import io.reactivex.Flowable;

public class AnnotatedPointRepository {

    private AnnotatedPointDao dao;
    private Flowable<List<AnnotatedPoint>> pointList;

    public AnnotatedPointRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getDatabase(application);
        dao = appDatabase.annotatedPointDao();
        pointList = dao.getAllAnnotatedPoint();
    }

    public Flowable<List<AnnotatedPoint>> getAnnotatedPointList() {
        return pointList;
    }

    public void addAnnotatedPoint(AnnotatedPoint annotatedPoint) {
        dao.addAnnotatedPoint(annotatedPoint);
    }

    public void updateAnnotatedPoint(AnnotatedPoint annotatedPoint) {
        dao.updateAnnotatedPoint(annotatedPoint);
    }

    public void removeAnnotatedPoint(AnnotatedPoint annotatedPoint) {
       dao.deleteAnnotatedPoint(annotatedPoint);
    }
}
