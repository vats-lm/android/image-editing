package com.example.imageediting.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.imageediting.database.entity.AnnotatedPoint;
import com.example.imageediting.repository.AnnotatedPointRepository;

import java.util.List;

import io.reactivex.Flowable;

public class MainActivityViewModel extends AndroidViewModel {

    private AnnotatedPointRepository repository;
    private Flowable<List<AnnotatedPoint>> annotatedPointListFlowable;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);

        repository = new AnnotatedPointRepository(application);
        annotatedPointListFlowable = repository.getAnnotatedPointList();
    }

    public Flowable<List<AnnotatedPoint>> getPointListFlowable() {
        return annotatedPointListFlowable;
    }

    public void addPoint(AnnotatedPoint annotatedPoint) {
        repository.addAnnotatedPoint(annotatedPoint);
    }

    public void updatePoint(AnnotatedPoint annotatedPoint) {
        repository.updateAnnotatedPoint(annotatedPoint);
    }

    public void removePoint(AnnotatedPoint annotatedPoint) {
        repository.removeAnnotatedPoint(annotatedPoint);
    }
}