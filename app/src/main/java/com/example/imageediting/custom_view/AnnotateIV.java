package com.example.imageediting.custom_view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.example.imageediting.database.entity.AnnotatedPoint;
import com.example.imageediting.view_model.MainActivityViewModel;

import java.util.List;

public class AnnotateIV extends AppCompatImageView {

    private static final int RADIUS = 50;
    private static final int TAP_RADIUS = 75;
    private static final int MAX_POINTS = 5;
    private float MAX_X;
    private float MAX_Y;
    private float MIN_X;
    private float MIN_Y;
    private boolean isBoundaryInitialized;
    private List<AnnotatedPoint> pointList;
    private Paint pointPaint;
    private boolean isMoving;
    private AnnotatedPoint movingPoint;
    private MainActivityViewModel viewModel;

    public AnnotateIV(Context context) {
        this(context, null);
    }

    public AnnotateIV(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnnotateIV(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        pointPaint = new Paint();
        pointPaint.setColor(Color.rgb(100, 100, 100));
        pointPaint.setStrokeWidth(2);
    }

    private void initBoundaries() {
        if (isBoundaryInitialized || this.getWidth() == 0 || this.getHeight() == 0)
            return;
        MAX_X = (float) (this.getWidth() - RADIUS - (RADIUS * .25));
        MAX_Y = (float) (this.getHeight() - RADIUS - (RADIUS * .25));
        MIN_X = (float) (RADIUS - (RADIUS * .25));
        MIN_Y = (float) (RADIUS - (RADIUS * .25));
        isBoundaryInitialized = true;
    }

    public void setPointList(List<AnnotatedPoint> pointList) {
        this.pointList = pointList;
        invalidate();
    }

    public void setViewModel(MainActivityViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initBoundaries();

        for (AnnotatedPoint point : pointList) {
            canvas.drawCircle(point.x * this.getWidth(), point.y * this.getHeight(), RADIUS, pointPaint);
        }
    }

    private AnnotatedPoint addPointFromEvent(MotionEvent event) {
        AnnotatedPoint annotatedPoint = new AnnotatedPoint();
        annotatedPoint.x = event.getX() / this.getWidth();
        annotatedPoint.y = event.getY() / this.getHeight();
        viewModel.addPoint(annotatedPoint);
        return annotatedPoint;
    }

    private AnnotatedPoint updateMovingPoint(MotionEvent event) {

        movingPoint.x = event.getX() / this.getWidth();
        movingPoint.y = event.getY() / this.getHeight();
        viewModel.updatePoint(movingPoint);
        return movingPoint;
    }

    private void removeMovingPoint() {
        viewModel.removePoint(movingPoint);
    }

    private AnnotatedPoint findAnnotatedPoint(float x, float y) {
        float leftTopMargin = TAP_RADIUS - RADIUS;
        for (AnnotatedPoint annotatedPoint : pointList) {
            float ax = (annotatedPoint.x * this.getWidth());
            float ay = (annotatedPoint.y * this.getHeight());
            if (x >= ax - leftTopMargin && x <= ax + TAP_RADIUS
                    && y >= ay - leftTopMargin && y <= ay + TAP_RADIUS) {
                return annotatedPoint;
            }
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN: {
                movingPoint = findAnnotatedPoint(x, y);
                if (movingPoint != null) {
                    isMoving = true;
                } else if (pointList.size() < MAX_POINTS) {
                    movingPoint = addPointFromEvent(event);
                    isMoving = true;
                }
                return true;
            }

            case MotionEvent.ACTION_MOVE: {
                if (isMoving) {
                    updateMovingPoint(event);
                }
                return true;
            }

            case MotionEvent.ACTION_UP: {
                if (isMoving)
                    if (x > MAX_X || x < MIN_X || y > MAX_Y || y < MIN_Y)
                        removeMovingPoint();

                isMoving = false;
                movingPoint = null;

                return true;
            }
        }

        return super.onTouchEvent(event);
    }
}
