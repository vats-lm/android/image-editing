package com.example.imageediting;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.imageediting.custom_view.AnnotateIV;
import com.example.imageediting.view_model.MainActivityViewModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private final CompositeDisposable disposable = new CompositeDisposable();
    private AnnotateIV imageView;
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);

        viewModel = ViewModelProviders.of(this)
                .get(MainActivityViewModel.class);

        imageView.setViewModel(viewModel);

        disposable.add(
                viewModel.getPointListFlowable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            imageView.setPointList(list);
                        })
        );
    }
}
