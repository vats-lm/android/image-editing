package com.example.imageediting.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.imageediting.database.entity.AnnotatedPoint;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface AnnotatedPointDao {

    @Query("Select * from AnnotatedPoint")
    Flowable<List<AnnotatedPoint>> getAllAnnotatedPoint();

    @Insert
    void addAnnotatedPoint(AnnotatedPoint annotatedPoint);

    @Update
    void updateAnnotatedPoint(AnnotatedPoint annotatedPoint);

    @Delete
    void deleteAnnotatedPoint(AnnotatedPoint annotatedPoint);
}
