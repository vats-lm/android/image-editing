package com.example.imageediting.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Objects;

@Entity
public class AnnotatedPoint {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "point_x")
    public float x;

    @ColumnInfo(name = "point_y")
    public float y;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnotatedPoint that = (AnnotatedPoint) o;
        return id == that.id &&
                Float.compare(that.x, x) == 0 &&
                Float.compare(that.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, x, y);
    }
}
